FROM python:3.11


COPY ../OPS2-API /app

COPY ../OPS2-API/requirements.txt /app/requirements.txt

RUN pip install --no-cache-dir -r /app/requirements.txt

EXPOSE 4242

CMD ["uvicorn", "main:app"]
